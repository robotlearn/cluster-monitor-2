#!/usr/bin/env bash

cd /local_scratch/galepage/cluster_monitor || exit 1

python -m http.server -d ./public &
trap 'trap - SIGTERM && kill -- -$$' SIGINT SIGTERM EXIT
python backend/fetch_data.py
