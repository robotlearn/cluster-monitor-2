#!/usr/bin/env bash

output_dir=/local_scratch/_state
mkdir -p "$output_dir"
rm -rf "${output_dir:?}"/*

####################################################################################################
# General info

timestamp=$(date +'%Y/%m/%d - %H:%M')
echo "
timestamp: '$timestamp'
cpu_threads: $(nproc --all)
total_mem: $(free -g | grep Mem: | awk '{print $2}')
mem_used: $(free -g | grep Mem: | awk '{print $3}')
" >"$output_dir"/stats.yaml

####################################################################################################
# GPU(s)

nvidia-smi >/dev/null || exit 1 # Exit if nvidia-smi is unreachable

n_gpus=$(nvidia-smi -L | wc -l)

# for gpu_id in $(seq 0 "$n_gpus"); do
for ((gpu_id = 0; gpu_id < "$n_gpus"; gpu_id++)); do
    gpu_model=$(nvidia-smi --query-gpu=gpu_name -i "$gpu_id" --format=csv,noheader)

    gpu_mem=$(nvidia-smi --query-gpu=memory.total -i "$gpu_id" --format=csv,noheader,nounits)

    gpu_jobs=$(nvidia-smi --query-compute-apps=pid,used_gpu_memory -i "$gpu_id" --format=csv,noheader,nounits)
    biggest_job=$(echo "$gpu_jobs" | sort -k2,2nr | head -n 1)
    biggest_job_pid=$(echo "$biggest_job" | cut -d " " -f1 | sed 's/,//g')
    gpu_user=""
    if [ -n "$biggest_job_pid" ]; then
        gpu_user=$(ps -o uname= -p "$biggest_job_pid")
    fi

    gpu_state_file="$output_dir"/gpu_"$gpu_id".yaml
    echo "
model: '$gpu_model'
memory: $gpu_mem
user: '$gpu_user'
id: $gpu_id
" >"$gpu_state_file"

done
