from os.path import isdir, join, dirname, abspath
from os import listdir
import yaml
import json
from dataclasses import dataclass
from typing import Any

from time import sleep

TIMEOUT_SEC: int = 1
GPU_MEM_MIN: int = 4  # GB

PROJECT_PATH: str = join(
    dirname(abspath(__file__)),
    "..",
)

OUTPUT_JSON_PATH: str = "public/stats.json"


def read_file(filepath: str) -> str:
    with open(filepath, "r") as f:
        content: str = f.read()

    return content.strip()


@dataclass
class Gpu:
    def __init__(
        self,
        model: str,
        mem_mb: int,
        id: int,
        user: str,
    ) -> None:
        self.id: int = id
        self.user: str = user

        model = model.replace("NVIDIA ", "")
        model = model.replace("Quadro ", "")
        model = model.replace("GeForce GTX ", "")
        self.model: str = model.strip()

        # Memory
        self.mem_gb: int = mem_mb // 1000
        if 0 < self.mem_gb < 8:
            pass
        elif 7 < self.mem_gb < 10:
            self.mem_gb = 8
        elif 10 < self.mem_gb < 14:
            self.mem_gb = 12
        elif 20 < self.mem_gb < 26:
            self.mem_gb = 24
        elif 44 < self.mem_gb < 50:
            self.mem_gb = 48
        else:
            assert False, f"Invalid GPU mem size: {self.mem_gb}GB"

    def to_dict(self) -> dict[str, Any]:
        return {
            "id": self.id,
            "model": self.model,
            "mem_gb": self.mem_gb,
            "busy": bool(self.user),
            "user": self.user,
        }


class Host:
    def __init__(
        self,
        hostname: str,
    ) -> None:
        state_path: str = f"/scratch/{hostname}/_state"

        self.is_cluster: bool = hostname.startswith("gpu")

        if not isdir(state_path):
            raise FileNotFoundError

        self.hostname: str = hostname

        ###########
        # Parsing #
        ###########
        with open(join(state_path, "stats.yaml"), "r") as f:
            stats_dict: dict[str, Any] = yaml.safe_load(f)

        self.timestamp: str = stats_dict["timestamp"]
        self.cpu_threads: int = stats_dict["cpu_threads"]
        self.total_mem: int = stats_dict["total_mem"]
        self.mem_used: int = stats_dict["mem_used"]

        # GPUs
        gpu_states: list[str] = list(
            filter(
                lambda node: node.startswith("gpu_"),
                listdir(state_path),
            ),
        )
        gpu_states.sort()

        self.gpus: list[Gpu] = []
        for gpu_state_file in gpu_states:
            gpu_stats_path: str = join(state_path, gpu_state_file)

            with open(gpu_stats_path, "r") as f:
                gpu_stats_dict: dict[str, Any] = yaml.safe_load(f)

            self.gpus.append(
                Gpu(
                    id=gpu_stats_dict["id"],
                    model=gpu_stats_dict["model"],
                    mem_mb=gpu_stats_dict["memory"],
                    user=gpu_stats_dict["user"],
                ),
            )

    def to_dict(self) -> dict[str, Any]:
        return {
            "hostname": self.hostname,
            "timestamp": self.timestamp,
            "cpu_threads": self.cpu_threads,
            "total_mem": self.total_mem,
            "mem_used": self.mem_used,
            "gpus": [gpu.to_dict() for gpu in self.gpus],
        }

    def has_decent_gpu(self) -> bool:
        for gpu in self.gpus:
            if gpu.mem_gb > GPU_MEM_MIN:
                return True
        return False


def fetch_all() -> None:
    print("\n###########################")
    hostnames_path: str = "backend/hostnames.txt"

    with open(hostnames_path, "r") as f:
        hostnames: list[str] = list(
            map(
                lambda h: h.strip(),
                list(f.readlines()),
            )
        )

    hosts: list[Host] = []
    for hostname in hostnames:
        print(f"Fetching state of host '{hostname}'")
        try:
            hosts.append(
                Host(hostname=hostname),
            )
        except (FileNotFoundError, TypeError):
            print(f"WARNING: fetching stats for host '{hostname}' failed.")

    hosts = list(
        filter(
            lambda h: h.has_decent_gpu(),
            hosts,
        ),
    )
    output_dict: dict[
        str,
        list[dict[str, Any]],
    ] = {
        "cluster": [h.to_dict() for h in hosts if h.is_cluster],
        "workstations": [h.to_dict() for h in hosts if (not h.is_cluster)],
    }

    # pprint(all_stats)
    with open("public/stats.json", "w") as f:
        json.dump(output_dict, f)


if __name__ == "__main__":
    assert isdir(dirname(OUTPUT_JSON_PATH))
    while True:
        fetch_all()
        sleep(TIMEOUT_SEC)
