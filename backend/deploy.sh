#!/usr/bin/env bash

if [ "$(hostname)" != "alya" ]; then
	echo "Please, run this from 'alya'"
	exit 1
fi

# systemd service on `alya`
echo "-> Enable systemd service on 'alya'"
sudo cp ./robotlearn-cluster-monitor.service /etc/systemd/system/
sudo systemctl enable --now robotlearn-cluster-monitor

# CRON job on all hosts
cron_script="cluster-monitor-cronjob"

echo -e "\n-> Deploy cron jobs on all workstations"
while read -r hostname; do
	echo "deploying cron job on host '$hostname'"
	rsync cluster_monitor_cron.sh "$hostname":/tmp/"$cron_script"

	cron_script_path=/etc/cron.d/"$cron_script"

	ssh -n "$hostname" "
        set -e
        sudo mv /tmp/'$cron_script' '$cron_script_path'
        sudo chown root:root '$cron_script_path'
        sudo crontab -l | grep -q '$cron_script_path' || (sudo crontab -l ; echo '* * * * * '$cron_script_path' > /dev/null') | sudo crontab -
    "
done <hostnames.txt

echo "Restart systemd service on 'alya'"
sudo systemctl restart robotlearn-cluster-monitor
