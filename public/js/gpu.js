import { Field, BoolField } from "./utils.js";


let USER_COLORS = {
    "DEFAULT": "#3b0057",
    "galepage": "#003966",
    "ollama": "#00541c",
    "aauterna": "#807a00",
    "ssadok": "#834c01",
    "lairale": "#45000f",
}


export function get_gpu_div(gpu_data) {
    let gpu_div = document.createElement('div');
    gpu_div.className = "gpu";

    // LABEL
    let model = gpu_data['model'];
    let mem = gpu_data['mem_gb'];
    let desc = `${model} (${mem}GB)`;
    let label = document.createElement('p');
    let id = gpu_data['id'];
    label.innerHTML = `<span style="color:var(--orange-moche)">GPU ${id}:</span> ${desc}`;
    gpu_div.appendChild(label);

    let busy = gpu_data['busy'];
    let state_div = document.createElement('div');
    if (busy) {
        let username = gpu_data['user'];

        let bg_color = USER_COLORS['DEFAULT'];
        if (username in USER_COLORS) {
            bg_color = USER_COLORS[username];
        }
        gpu_div.style.background = bg_color;
        state_div.className = "container";
        let busy_par = document.createElement('p');
        busy_par.innerHTML = `busy (@${username})`;
        state_div.appendChild(busy_par);
    } else {
        state_div.className = "free container";
        let free_par = document.createElement('p');
        free_par.className = "free-text";
        free_par.innerHTML = '<strong>FREE</strong>';
        state_div.appendChild(free_par);
    }
    gpu_div.appendChild(state_div);

    return gpu_div;
}
