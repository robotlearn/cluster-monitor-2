import * as utils from "./utils.js";
import { show_node } from "./node.js";
// import stats from "../stats.json" assert { type: 'json' };


function display_nodes(nodes, main_container) {
    for (let node_data of nodes) {
        let node = show_node(node_data)
        main_container.appendChild(node)
    }
}

export function cluster(cluster_data) {
    let main_container = document.createElement('div');
    main_container.className = 'main container';
    main_container.id = 'cluster';

    display_nodes(cluster_data["cluster"], main_container)
    display_nodes(cluster_data["workstations"], main_container)

    return main_container;
}
