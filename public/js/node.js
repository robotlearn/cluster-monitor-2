import { get_gpu_div } from "./gpu.js";


export function show_node(node_data) {
    let node_div = document.createElement('div');
    node_div.className = 'node container';

    let div_name = document.createElement('div')
    div_name.className = 'node-title container centered';
    let par_name = document.createElement('p');
    par_name.className = 'hostname';
    par_name.innerHTML = node_data['hostname'];
    div_name.append(par_name);

    node_div.appendChild(div_name);

    let gpus = node_data['gpus'];
    for (const gpu of gpus) {
        let gpu_div = get_gpu_div(gpu);
        node_div.appendChild(gpu_div);
    }

    return node_div;
}
