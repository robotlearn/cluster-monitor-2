import { Field } from "./utils.js";
import { cluster } from "./cluster.js";

var last_updated = new Field('Last update', '', false);
last_updated.get_node_from_id('last-updated');

let root = document.getElementById('home');
let cluster_div = null;

function update_timestamp() {
    let currentDate = new Date();
    let current_time = currentDate.getHours() + ':';
    current_time += String(
        currentDate.getMinutes()
    ).padStart(2, '0') + ":";
    current_time += String(
        currentDate.getSeconds()
    ).padStart(2, '0');
    last_updated.set_text(current_time);
}

function display(data) {
    update_timestamp();
    if (cluster_div != null) {
        root.removeChild(cluster_div);
    }
    cluster_div = cluster(data);
    root.appendChild(cluster_div);
}

function main() {
    $.getJSON("/stats.json", display);
}

main()
setInterval(main, 5000); // interval is in milliseconds
