export class DivField {
    constructor(div_class, field) {
        this.div = document.createElement('div');
        this.div.className = div_class;

        this.field = field;
        this.div.appendChild(this.field.node);
    }
}


export class Field {
    constructor(
        name = 'default',
        text = 'default',
        create_node = true,
        css_class = undefined
    ) {

        this.name = name;
        this.text = text;
        this.css_class = css_class;

        if ( create_node ) {
            this.node = document.createElement('p');
            this.set_node_properties();
        }
    }

    set_node_properties() {
        this.set_text()
        if ( this.css_class !== undefined )
            this.node.className = this.css_class;
    }

    set_text(text = this.text) {
        this.text = text;
        this.node.innerHTML = `<span style="color:var(--orange-moche)">${this.name}:</span> ${this.text}`;
    }

    get_node_from_id(id) {
        this.node = document.getElementById(id);
    }
}


export class BoolField extends Field {
    constructor(name, class_name) {
        super(name, 'x', true, class_name);
        this.enabled_char = '✅';
        this.disabled_char = '❌';
    }

    update(enabled) {
        if ( enabled ) {
            this.set_text(this.enabled_char);
        }
        else {
            this.set_text(this.disabled_char);
        }
    }
}
