# cluster-monitor-2


- Backend: `backend/`:
    - `cluster_monitor_cron.sh`: the cron job script ran by each node to publish its state on its local drive.
    - `robotlearn-cluster-monitor.service`: Systemd service that runs on the _master_ node.
        It reads the state of each node and hosts an aggregated JSON.
- Frontend: `public/`:
    - Very basic HMTL/CSS/JS that produces a decent layout from the raw `stats.json` data.
